const isProd = process.env.NODE_ENV === 'production';
const isDev = !isProd;

module.exports = {
    plugins: [
        '@babel/plugin-proposal-class-properties',
        [
            'emotion',
            {
                hoist: isProd,
                sourceMap: isDev,
                autoLabel: isDev
            }
        ]
    ],
    presets: [
        [
            '@babel/preset-env',
            {
                useBuiltIns: 'entry'
            }
        ],
        '@babel/preset-react'
    ]
};
