# Occupy Stall Street - Chrome Extension

Uses the Occupy Stall Street sensors to indicate which bathroom stalls are available.

## Dev Setup
You're going to need Node.js. Any version >= 8 should be fine.

1. Clone this repo.
2. `yarn` to install dependencies.
3. `yarn start` to build the dev version and watch for changes.
4. Install into Chrome:
    1. Go to the Chrome extensions page.
    2. Turn on Developer mode.
    3. Add the `dist` folder as a new extension.
5. `yarn run build` to build the deployable version.

## Contributing
If you have ideas, submit them in the [issue tracker](https://bitbucket.org/_bsokol/occupy-stall-street-chrome/issues?status=new&status=open).

If you want to make changes, just open a pull request. I'll submit the updated extension whenever changes are merged.
