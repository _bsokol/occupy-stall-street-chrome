import PropTypes from 'prop-types';
import React from 'react';
import { css, cx } from 'react-emotion';
import { colorGray, colorGreen, colorMediumGray, colorWhite } from '../common/styles';

const cellClass = css`
    width: 100%;
    font-size: 24px;
    padding: 0;
    height: 50px;
    width: 175px;
    line-height: 50px;
    text-align: center;
    background: ${colorGray};
    color: ${colorWhite};
    border: none;
    margin: 0;
`;

const almostActiveClass = css`
    background: ${colorMediumGray};
`;

const activeClass = css`
    background: ${colorGreen};
`;

export function StatusCell({ floorStatus, genderStatus }) {
    return (
        <div className={cx(cellClass, { [almostActiveClass]: floorStatus || genderStatus, [activeClass]: floorStatus && genderStatus })}>
            {floorStatus && genderStatus ? 'Shown' : 'Hidden'}
        </div>
    );
}

StatusCell.propTypes = {
    floorStatus: PropTypes.bool.isRequired,
    genderStatus: PropTypes.bool.isRequired
};
