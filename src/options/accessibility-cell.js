import PropTypes from 'prop-types';
import React from 'react';
import { css, cx } from 'react-emotion';
import handicappedIcon from '../assets/img/ic_accessible_new_white_505px.svg';
import { colorBlue, colorRed, colorWhite } from '../common/styles';

const buttonClass = css`
    width: 150px;
    font-size: 24px;
    font-weight: bold;
    padding: 0;
    height: 50px;
    line-height: 50px;
    text-decoration: underline;
    text-align: center;
    background-color: ${colorRed};
    color: ${colorWhite};
    border: none;
    margin: 0;
    cursor: pointer;
    &:disabled {
        background-color: ${colorBlue};
        text-decoration: none;
        cursor: default;
    }
`;

const iconClass = css`
    display: inline-block;
    background-image: url('${handicappedIcon}');
    background-repeat: no-repeat;
    background-position: 0 7px;
    background-size: 28px;
    padding-left: 40px;
    text-align: left;
    text-decoration: underline;
`;

const activeIconClass = css`
    text-decoration: none;
`;

function saveOption(type, currentOptions) {
    window.chrome.storage.local.set({
        options: {
            ...currentOptions,
            onlyHandicapped: !currentOptions.onlyHandicapped
        }
    });
}

export function AccessibilityCell({ type, currentOptions }) {
    return (
        <button
            className={buttonClass}
            onClick={() => saveOption(type, currentOptions)}
            disabled={type === 'all' ? !currentOptions.onlyHandicapped : currentOptions.onlyHandicapped}
        >
            {type === 'all' ? (
                'All'
            ) : (
                <div
                    className={cx(iconClass, {
                        [activeIconClass]: type === 'all' ? !currentOptions.onlyHandicapped : currentOptions.onlyHandicapped
                    })}
                >
                    Only
                </div>
            )}
        </button>
    );
}

AccessibilityCell.propTypes = {
    type: PropTypes.string.isRequired,
    currentOptions: PropTypes.object.isRequired
};
