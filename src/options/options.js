import '@babel/polyfill';
import React from 'react';
import { render } from 'react-dom';
import { css } from 'react-emotion';
import { colorBlue, colorWhite, setGlobalStyles } from '../common/styles';
import { updateBadge } from '../common/utils';
import { AccessibilityCell } from './accessibility-cell';
import { OptionCell } from './option-cell';
import { StatusCell } from './status-cell';

setGlobalStyles();

const headerClass = css`
    background: ${colorBlue};
    color: ${colorWhite};
    font-weight: 200;
    font-size: 30px;
    letter-spacing: 1px;
    padding: 10px 15px;
    text-align: center;
`;

const instructionsClass = css`
    color: ${colorWhite};
    font-size: 16px;
    padding: 50px 15px 20px;
    text-align: center;
`;

const tableClass = css`
    border-collapse: collapse;
    margin: 0 auto;
`;

class OptionsPage extends React.Component {
    constructor() {
        super();

        this.componentDidMount = this.componentDidMount.bind(this);
        this.onUpdate = this.onUpdate.bind(this);

        this.state = {
            ready: false,
            fullData: {},
            visibleFloors: {},
            visibleGenders: {},
            options: {},
            notifications: {}
        };
    }

    componentDidMount() {
        window.chrome.storage.onChanged.addListener(this.onUpdate);
        window.chrome.storage.local.get(['fullData', 'visibleFloors', 'visibleGenders', 'options', 'notifications'], result => {
            this.setState({
                ready: true,
                fullData: result.fullData,
                visibleFloors: result.visibleFloors,
                visibleGenders: result.visibleGenders,
                options: result.options,
                notifications: result.notifications
            });
        });
    }

    componentWillUnmount() {
        window.chrome.storage.onChanged.removeListener(this.onUpdate);
    }

    onUpdate(changes) {
        if (changes.visibleFloors && JSON.stringify(changes.visibleFloors.oldValue) !== JSON.stringify(changes.visibleFloors.newValue)) {
            this.setState({
                visibleFloors: changes.visibleFloors.newValue
            });
        }
        if (changes.visibleGenders && JSON.stringify(changes.visibleGenders.oldValue) !== JSON.stringify(changes.visibleGenders.newValue)) {
            this.setState({
                visibleGenders: changes.visibleGenders.newValue
            });
        }
        if (changes.options && JSON.stringify(changes.options.oldValue) !== JSON.stringify(changes.options.newValue)) {
            this.setState({
                options: changes.options.newValue
            });
        }
        if (changes.notifications && JSON.stringify(changes.notifications.oldValue) !== JSON.stringify(changes.notifications.newValue)) {
            this.setState({
                notifications: changes.notifications.newValue
            });
        }
        updateBadge();
    }

    render() {
        return (
            <>
                <h1 className={headerClass}>Occupy Stall Street Options</h1>

                <div className={instructionsClass}>Which stalls would you like to see?</div>
                <table className={tableClass}>
                    <tbody>
                        <tr>
                            <td>
                                <AccessibilityCell type="all" currentOptions={this.state.options} />
                            </td>
                            <td>
                                <AccessibilityCell type="handicapped" currentOptions={this.state.options} />
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div className={instructionsClass}>Which bathrooms would you like to see?</div>
                {this.state.ready ? (
                    <table className={tableClass}>
                        <tbody>
                            <tr>
                                <td />
                                <td>
                                    <OptionCell
                                        type="visibleGenders"
                                        label="Women"
                                        option="gender-women"
                                        status={this.state.visibleGenders['gender-women']}
                                        currentOptions={this.state.visibleGenders}
                                    />
                                </td>
                                <td>
                                    <OptionCell
                                        type="visibleGenders"
                                        label="Men"
                                        option="gender-men"
                                        status={this.state.visibleGenders['gender-men']}
                                        currentOptions={this.state.visibleGenders}
                                    />
                                </td>
                            </tr>
                            {Object.keys(this.state.fullData).map(floor => (
                                <tr key={floor}>
                                    <td>
                                        <OptionCell
                                            type="visibleFloors"
                                            label={this.state.fullData[floor].label}
                                            option={this.state.fullData[floor].option}
                                            status={this.state.visibleFloors[this.state.fullData[floor].option]}
                                            currentOptions={this.state.visibleFloors}
                                            alignRight
                                        />
                                    </td>
                                    <td>
                                        <StatusCell
                                            floorStatus={this.state.visibleFloors[this.state.fullData[floor].option]}
                                            genderStatus={this.state.visibleGenders['gender-women']}
                                        />
                                    </td>
                                    <td>
                                        <StatusCell
                                            floorStatus={this.state.visibleFloors[this.state.fullData[floor].option]}
                                            genderStatus={this.state.visibleGenders['gender-men']}
                                        />
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                ) : null}
            </>
        );
    }
}

render(<OptionsPage />, document.getElementById('extension'));
