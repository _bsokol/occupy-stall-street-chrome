import PropTypes from 'prop-types';
import React from 'react';
import { css, cx } from 'react-emotion';
import { colorBlue, colorRed, colorWhite } from '../common/styles';
import { updateBadge } from '../common/utils';

const buttonClass = css`
    width: 100%;
    font-size: 24px;
    font-weight: bold;
    padding: 0 35px;
    height: 50px;
    line-height: 50px;
    text-decoration: underline;
    text-align: center;
    background: ${colorRed};
    color: ${colorWhite};
    border: none;
    margin: 0;
    cursor: pointer;
`;

const rightClass = css`
    text-align: right;
`;

const activeClass = css`
    background: ${colorBlue};
`;

function saveOption(type, option, status, currentOptions) {
    window.chrome.storage.local.set(
        {
            [type]: {
                ...currentOptions,
                [option]: !status
            }
        },
        updateBadge
    );
}

export function OptionCell({ type, label, option, status, currentOptions, alignRight }) {
    return (
        <button
            className={cx(buttonClass, { [rightClass]: alignRight, [activeClass]: status })}
            onClick={() => saveOption(type, option, status, currentOptions)}
        >
            {label}
        </button>
    );
}

OptionCell.propTypes = {
    type: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    option: PropTypes.string.isRequired,
    status: PropTypes.bool.isRequired,
    currentOptions: PropTypes.object.isRequired,
    alignRight: PropTypes.bool
};
