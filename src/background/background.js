import '@babel/polyfill';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import extensionIcon from '../assets/static/icon128.png';
import { extractFloorOptions, extractGenderOptions, getVisibleStalls, parseData, updateBadge } from '../common/utils';

let db;
let rootRef;
let connectedRef;
let pendingNotificationChecks = 0;

// window.chrome.storage.local.clear();

function init() {
    const config = {
        apiKey: 'AIzaSyDuC9MaCtGDKDLIufC40X2q2TkGHF6Krcw',
        authDomain: 'stallmonitor.firebaseapp.com',
        databaseURL: 'https://stallmonitor.firebaseio.com',
        projectId: 'stallmonitor',
        storageBucket: 'stallmonitor.appspot.com',
        messagingSenderId: '588195377816'
    };
    console.log(`Using Firebase SDK version ${firebase.SDK_VERSION}`);
    return firebase.initializeApp(config);
}

function updateConnectionStatus(snapshot) {
    window.chrome.storage.local.get(['connected'], result => {
        if (!result.connected && snapshot.val()) {
            rootRef = db.ref();
            rootRef.on('value', processSnapshot);
        }
        window.chrome.storage.local.set({ connected: snapshot.val() });
    });
}

function testConnection() {
    try {
        connectedRef = db.ref('.info/connected');
        connectedRef.on('value', testSnapshot => {
            if (testSnapshot.val() !== true) {
                rootRef = db.ref();
                rootRef.on('value', processSnapshot);
            }
            window.chrome.storage.local.set({ connected: testSnapshot.val() });
        });
    } catch (e) {
        window.chrome.storage.local.set({ connected: false });
    }
}

function processSnapshot(dataSnapshot) {
    // console.log('RAW DATA', dataSnapshot.toJSON());
    // console.log('PARSED DATA', parseData(dataSnapshot.toJSON()));

    // const testData = dataSnapshot.toJSON();
    // testData['38'] = {
    //     ...dataSnapshot.toJSON()['51'],
    //     Location: {
    //         meta: true,
    //         name: 'aon-38'
    //     }
    // };

    // console.log('testData', testData);

    const fullData = parseData(dataSnapshot.toJSON());
    // fullData['two-prudential-51'].genders.men.stalls[1].occupied = true;
    // fullData['two-prudential-51'].genders.men.stalls[0].occupied = true;
    const floorOptions = extractFloorOptions(fullData);
    const genderOptions = extractGenderOptions();

    window.chrome.storage.local.set({ fullData });

    window.chrome.storage.local.get(['visibleFloors', 'visibleGenders', 'options', 'notifications'], results => {
        const visibleFloors = floorOptions.reduce((floors, floorOption) => {
            floors[floorOption] =
                results.visibleFloors && typeof results.visibleFloors[floorOption] === 'boolean'
                    ? results.visibleFloors[floorOption]
                    : true;
            return floors;
        }, {});
        const visibleGenders = genderOptions.reduce((genders, genderOption) => {
            genders[genderOption] =
                results.visibleGenders && typeof results.visibleGenders[genderOption] === 'boolean'
                    ? results.visibleGenders[genderOption]
                    : true;
            return genders;
        }, {});
        window.chrome.storage.local.set({ visibleFloors, visibleGenders }, updateBadge);
        pendingNotificationChecks++;
        if (pendingNotificationChecks === 1) {
            sendNotifications(fullData, visibleFloors, visibleGenders, results.options, results.notifications);
        }
    });
}

function sendNotifications(fullData, visibleFloors, visibleGenders, options, notifications) {
    const visibleStalls = getVisibleStalls(fullData, visibleFloors, visibleGenders, options);

    notifications.forEach((alert, i) => {
        try {
            const gender = Object.values(Object.values(visibleStalls).find(floor => floor.option === alert.floorOption).genders).find(
                gender => gender.option === alert.genderOption
            );
            const active = gender.stalls.reduce((status, stall) => status || stall.active, false);
            const occupied = gender.stalls.reduce((status, stall) => status && stall.occupied, true);

            if (active && !occupied) {
                notifications[i].done = true;
                window.chrome.notifications.create(null, {
                    type: 'basic',
                    iconUrl: extensionIcon,
                    title: 'Occupy Stall Street',
                    message: 'A stall is now available',
                    requireInteraction: true
                });
            }
        } catch (e) {
            notifications[i].done = true;
        }
    });

    window.chrome.storage.local.set(
        {
            notifications: notifications.filter(alert => !alert.done)
        },
        () => {
            pendingNotificationChecks--;
            if (pendingNotificationChecks > 0) {
                window.chrome.storage.local.get(['fullData', 'visibleFloors', 'visibleGenders', 'options', 'notifications'], results => {
                    sendNotifications(
                        results.fullData,
                        results.visibleFloors,
                        results.visibleGenders,
                        results.options,
                        results.notifications
                    );
                });
            }
        }
    );
}

function start() {
    db = firebase.database(init());
    connectedRef = db.ref('.info/connected');
    connectedRef.on('value', updateConnectionStatus);
    rootRef = db.ref();
    rootRef.on('value', processSnapshot);
    window.chrome.alarms.onAlarm.addListener(testConnection);
    window.chrome.alarms.create('testConnection', { periodInMinutes: 1 });
}

window.chrome.storage.local.get(null, localResults => {
    if (typeof localResults['notifications'] === 'undefined') {
        window.chrome.storage.local.set({
            notifications: []
        });
    }

    if (typeof localResults['options'] === 'undefined') {
        window.chrome.storage.local.set({
            options: {}
        });
    }

    if (typeof localResults['connected'] === 'undefined') {
        window.chrome.storage.local.set({
            connected: false
        });
    }
});

window.chrome.storage.sync.get(null, syncResults => {
    if (typeof syncResults['Floor-51'] !== 'undefined') {
        window.chrome.storage.sync.clear(() => {
            window.chrome.storage.local.get(null, localResults => {
                if (typeof localResults.data !== 'undefined') {
                    window.chrome.storage.local.clear(start);
                } else {
                    start();
                }
            });
        });
    } else {
        window.chrome.storage.local.get(null, localResults => {
            if (typeof localResults.data !== 'undefined') {
                window.chrome.storage.local.clear(start);
            } else {
                start();
            }
        });
    }
});

// setInterval(() => {
//     window.chrome.storage.local.get(['fullData'], results => {
//         if (Math.random() > 0.5) {
//             results.fullData['two-prudential-51'].genders.men.stalls[0].occupied = !results.fullData['two-prudential-51'].genders.men
//                 .stalls[0].occupied;
//         }
//         if (Math.random() > 0.5) {
//             results.fullData['two-prudential-51'].genders.men.stalls[1].occupied = !results.fullData['two-prudential-51'].genders.men
//                 .stalls[1].occupied;
//         }
//         window.chrome.storage.local.set({ fullData: results.fullData }, () => {
//             updateBadge();
//             window.chrome.storage.local.get(['visibleFloors', 'visibleGenders', 'options', 'notifications'], alertResults => {
//                 sendNotifications(
//                     results.fullData,
//                     alertResults.visibleFloors,
//                     alertResults.visibleGenders,
//                     alertResults.options,
//                     alertResults.notifications
//                 );
//             });
//         });
//     });
// }, 10000);
