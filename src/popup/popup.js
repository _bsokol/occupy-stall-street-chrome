import '@babel/polyfill';
import React from 'react';
import { render } from 'react-dom';
import { css } from 'react-emotion';
import optionsIcon from '../assets/img/ic_settings_white_24px.svg';
import disconnectedIcon from '../assets/img/ic_inactive_white_24px.svg';
import { colorBlue, colorGray, colorMediumGray, colorWhite, setGlobalStyles } from '../common/styles';
import { countVisibleStalls, getVisibleStalls } from '../common/utils';
import { Gender } from './gender';

setGlobalStyles();

const headerClass = css`
    position: relative;
    background: ${colorBlue};
    color: ${colorWhite};
    font-weight: 200;
    font-size: 20px;
    letter-spacing: 1px;
    padding: 0 50px 0 15px;
    height: 40px;
    line-height: 40px;
    text-align: left;
`;

const optionsClass = css`
    position: absolute;
    top: 0;
    right: 0;
    width: 40px;
    height: 40px;
    background: url('${optionsIcon}') no-repeat center;
    background-size: 20px 20px;
    border: none;
    cursor: pointer;
`;

const disconnectedClass = css`
    position: absolute;
    top: 0;
    right: 40px;
    width: 20px;
    height: 40px;
    background: url('${disconnectedIcon}') no-repeat center;
    background-size: 20px 20px;
    border: none;
    cursor: pointer;
`;

const noDataClass = css`
    width: 350px;
    height: 150px;
    display: flex;
    align-content: center;
    align-items: center;
`;

const noDataMessageClass = css`
    flex: 1 1 auto;
    text-align: center;
    font-size: 30px;
    font-weight: 500;
    color: ${colorMediumGray};
    line-height: 50px;
`;

const floorTitleClass = css`
    font-size: 18px;
    font-weight: 400;
    color: ${colorGray};
    padding: 20px 15px 10px;
    text-transform: uppercase;
`;

const genderContainerClass = css`
    display: flex;
`;

class Popup extends React.Component {
    static openOptions() {
        window.open(window.chrome.extension.getURL('options.html'), '_blank');
    }

    state = {
        connected: false,
        visibleData: {},
        visibleFloors: {},
        visibleGenders: {},
        options: {},
        notifications: []
    };

    constructor() {
        super();

        this.componentDidMount = this.componentDidMount.bind(this);
        this.componentWillUnmount = this.componentWillUnmount.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
    }

    componentDidMount() {
        window.chrome.storage.onChanged.addListener(this.onUpdate);
        window.chrome.storage.local.get(
            ['connected', 'fullData', 'visibleFloors', 'visibleGenders', 'options', 'notifications'],
            results => {
                this.setState({
                    connected: results.connected,
                    visibleData: getVisibleStalls(results.fullData, results.visibleFloors, results.visibleGenders, results.options),
                    visibleFloors: results.visibleFloors,
                    visibleGenders: results.visibleGenders,
                    options: results.options,
                    notifications: results.notifications
                });
            }
        );
    }

    componentWillUnmount() {
        window.chrome.storage.onChanged.removeListener(this.onUpdate);
    }

    onUpdate(changes) {
        if (
            (changes.fullData && JSON.stringify(changes.fullData.oldValue) !== JSON.stringify(changes.fullData.newValue)) ||
            (changes.options && JSON.stringify(changes.options.oldValue) !== JSON.stringify(changes.options.newValue))
        ) {
            this.setState({
                visibleData: getVisibleStalls(
                    changes.fullData ? changes.fullData.newValue : this.state.fullData,
                    this.state.visibleFloors,
                    this.state.visibleGenders,
                    changes.options ? changes.options.newValue : this.state.options
                )
            });
        }
        if (changes.connected && JSON.stringify(changes.connected.oldValue) !== JSON.stringify(changes.connected.newValue)) {
            this.setState({
                connected: changes.connected.newValue
            });
        }
        if (changes.notifications && JSON.stringify(changes.notifications.oldValue) !== JSON.stringify(changes.notifications.newValue)) {
            this.setState({
                notifications: changes.notifications.newValue
            });
        }
    }

    render() {
        const { connected, visibleData, visibleFloors, visibleGenders, options, notifications } = this.state;

        return (
            <>
                <h1 className={headerClass}>
                    Occupy Stall Street
                    {!connected && <div className={disconnectedClass} />}
                    <button className={optionsClass} onClick={Popup.openOptions} />
                </h1>
                {!countVisibleStalls(visibleData, visibleFloors, visibleGenders, options) ? (
                    <div className={noDataClass}>
                        <h2 className={noDataMessageClass}>
                            ¯\_(ツ)_/¯
                            <br />
                            Nothing to Show
                        </h2>
                    </div>
                ) : (
                    <>
                        {Object.values(visibleData).map(floor => (
                            <div key={floor.option}>
                                <h2 className={floorTitleClass}>{floor.label}</h2>
                                <div className={genderContainerClass}>
                                    {visibleGenders['gender-women'] &&
                                        floor.genders.women && (
                                            <Gender
                                                option={floor.genders.women.option}
                                                label={floor.genders.women.label}
                                                stalls={floor.genders.women.stalls}
                                                floorOption={floor.option}
                                                notifications={notifications}
                                            />
                                        )}
                                    {visibleGenders['gender-men'] &&
                                        floor.genders.men && (
                                            <Gender
                                                option={floor.genders.men.option}
                                                label={floor.genders.men.label}
                                                stalls={floor.genders.men.stalls}
                                                floorOption={floor.option}
                                                notifications={notifications}
                                            />
                                        )}
                                </div>
                            </div>
                        ))}
                    </>
                )}
            </>
        );
    }
}

render(<Popup />, document.getElementById('extension'));
