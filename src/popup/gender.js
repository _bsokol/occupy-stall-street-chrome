import PropTypes from 'prop-types';
import React from 'react';
import { css, cx } from 'react-emotion';
import alertIcon from '../assets/img/ic_alert_white_25px.svg';
import { colorGreen, colorMediumGray, colorRed, colorWhite } from '../common/styles';
import { addNotification, hasNotification, removeNotification } from '../common/utils';
import { Stall } from './stall';

const genderContainerClass = css`
    flex: 1 1 auto;
    width: 300px;
    margin: 0 2px;
    &:first-child {
        margin-left: 0;
    }
    &:last-child {
        margin-right: 0;
    }
`;

const genderTitleClass = css`
    position: relative;
    font-size: 20px;
    font-weight: 400;
    color: ${colorWhite};
    background: ${colorMediumGray};
    padding: 10px 35px 10px 15px;
    margin-bottom: 2px;
    transition: all 0.5s ease-in-out;
`;

const alertIconClass = css`
    position: absolute;
    font-size: 0;
    color: transparent;
    top: 50%;
    right: 5px;
    width: 30px;
    height: 30px;
    transform: translateY(-50%);
    border-radius: 50%;
    background: url('${alertIcon}') no-repeat center;
    background-size: 18px 18px;
    border: none;
    cursor: pointer;
    transition: background-color 0.1s ease-in-out;
`;

const alertIconSetClass = css`
    background-color: rgba(256, 256, 256, 0.3);
`;

const availableClass = css`
    background: ${colorGreen};
`;

const occupiedClass = css`
    background: ${colorRed};
`;

function toggleAlert(notifications, floorOption, genderOption) {
    let pendingNotifications = [...notifications];

    if (hasNotification(pendingNotifications, floorOption, genderOption)) {
        pendingNotifications = removeNotification(pendingNotifications, floorOption, genderOption);
    } else {
        pendingNotifications = addNotification(pendingNotifications, floorOption, genderOption);
    }

    window.chrome.storage.local.set({
        notifications: pendingNotifications
    });
}

export function Gender({ label, option, stalls, floorOption, notifications }) {
    const active = stalls.reduce((status, stall) => status || stall.active, false);
    const occupied = stalls.reduce((status, stall) => status && stall.occupied, true);
    const alertSet = hasNotification(notifications, floorOption, option);

    return (
        <div className={genderContainerClass}>
            <h3 className={cx(genderTitleClass, { [availableClass]: active && !occupied, [occupiedClass]: active && occupied })}>
                {label}
                {(active && occupied) || alertSet ? (
                    <button
                        className={cx(alertIconClass, { [alertIconSetClass]: alertSet })}
                        onClick={() => toggleAlert(notifications, floorOption, option, alertSet)}
                    >
                        Request Notification
                    </button>
                ) : null}
            </h3>
            {stalls.map(stall => (
                <Stall
                    key={stall.label}
                    label={stall.label}
                    active={stall.active}
                    occupied={stall.occupied}
                    handicapped={stall.handicapped}
                />
            ))}
        </div>
    );
}

Gender.propTypes = {
    label: PropTypes.string.isRequired,
    option: PropTypes.string.isRequired,
    stalls: PropTypes.array.isRequired,
    floorOption: PropTypes.string.isRequired,
    notifications: PropTypes.array.isRequired
};
