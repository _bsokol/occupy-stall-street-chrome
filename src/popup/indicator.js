import PropTypes from 'prop-types';
import React from 'react';
import { css, cx, keyframes } from 'react-emotion';
import accessibleSvg from '../assets/img/ic_accessible_new_white_505px.svg';
import { colorBackground, colorGreen, colorMediumGray, colorRed, colorShadow, colorWhite } from '../common/styles';

const inactiveDoorKeyframes = keyframes`
    0%, 15%, 25%, 100% {transform: rotateY(0deg);}
    10%, 20% {transform: rotateY(10deg);}
`;

const inactiveShadowKeyframes = keyframes`
    0%, 15%, 25%, 100% {left: 100%;}
    10%, 20% {left: 70%;}
`;

const indicatorContainerClass = css`
    position: absolute;
    top: -2px;
    left: 10px;
    width: 50px;
    height: 80px;
    margin: 2px 0;
    background: ${colorBackground};
    perspective: 80px;
    transition: all 0.5s ease-in-out;
`;

const doorClass = css`
    position: absolute;
    top: 0;
    left: 1px;
    transition: all 0.5s ease-in-out;
    transform-style: preserve-3d;
    transform-origin: left center;
    width: 48px;
    height: 80px;
    color: ${colorWhite};
    background: ${colorMediumGray};
    overflow: hidden;
`;

const availableClass = css`
    background: ${colorGreen};
    transform: rotateY(45deg);
`;

const occupiedClass = css`
    background: ${colorRed};
`;

const inactiveClass = css`
    animation: ${inactiveDoorKeyframes} 2s linear infinite;
`;

const accessibleClass = css`
    position: absolute;
    bottom: 50%;
    left: 50%;
    width: 25px;
    transform: translateX(-55%) translateY(30%);
`;

const doorKnobClass = css`
    position: absolute;
    top: 50%;
    right: 4px;
    width: 5px;
    height: 5px;
    border-radius: 100%;
    background: ${colorWhite};
`;

const shadowClass = css`
    position: absolute;
    top: 0;
    left: 100%;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(to right, transparent, ${colorShadow});
    transition: all 0.5s ease-in-out;
`;

const shadowVisibleClass = css`
    left: 1px;
`;

const shadowInactiveClass = css`
    animation: ${inactiveShadowKeyframes} 2s linear infinite;
`;

export function Indicator({ occupied, active, handicapped }) {
    return (
        <div className={indicatorContainerClass}>
            <div
                className={cx(doorClass, {
                    [availableClass]: active && !occupied,
                    [occupiedClass]: active && occupied,
                    [inactiveClass]: !active
                })}
            >
                <div className={doorKnobClass} />
                {handicapped && <img src={accessibleSvg} className={accessibleClass} />}
                <div className={cx(shadowClass, { [shadowVisibleClass]: active && !occupied, [shadowInactiveClass]: !active })} />
            </div>
        </div>
    );
}

Indicator.propTypes = {
    occupied: PropTypes.bool.isRequired,
    active: PropTypes.bool.isRequired,
    handicapped: PropTypes.bool.isRequired
};
