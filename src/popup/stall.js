import PropTypes from 'prop-types';
import React from 'react';
import { css, cx } from 'react-emotion';
import inactiveSvg from '../assets/img/ic_inactive_white_24px.svg';
import { colorGreen, colorMediumGray, colorRed, colorWhite } from '../common/styles';
import { Indicator } from './indicator';

const stallContainerClass = css`
    position: relative;
    width: 100%;
    height: 80px;
    margin: 1px 0;
    color: ${colorWhite};
    background: ${colorMediumGray};
`;

const availableClass = css`
    background: ${colorGreen};
`;

const occupiedClass = css`
    background: ${colorRed};
`;

const stallLabelClass = css`
    font-size: 24px;
    font-weight: 300;
    line-height: 80px;
    padding-left: 90px;
    transition: all 0.5s ease-in-out;
`;

const inactiveIconClass = css`
    position: relative;
    top: 2px;
    left: 6px;
`;

export function Stall({ label, occupied, active, handicapped }) {
    return (
        <div className={stallContainerClass}>
            <h3 className={cx(stallLabelClass, { [availableClass]: active && !occupied, [occupiedClass]: active && occupied })}>
                {label}
                {!active ? <img className={inactiveIconClass} src={inactiveSvg} /> : ''}
            </h3>
            <Indicator active={active} occupied={occupied} handicapped={handicapped} />
        </div>
    );
}

Stall.propTypes = {
    label: PropTypes.string.isRequired,
    occupied: PropTypes.bool.isRequired,
    active: PropTypes.bool.isRequired,
    handicapped: PropTypes.bool.isRequired
};
