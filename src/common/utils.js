import startCase from 'lodash/startCase';

export function parseData(data) {
    const parsedData = {};

    Object.keys(data).forEach(floorNumber => {
        parsedData[data[floorNumber].Location.name] = {
            label: startCase(data[floorNumber].Location.name),
            option: `floor-${data[floorNumber].Location.name}`,
            genders: {
                men: {
                    label: 'Men',
                    option: 'gender-men',
                    stalls: data[floorNumber].Men
                        ? Object.keys(data[floorNumber].Men.spaces).map(space => {
                              return {
                                  label: startCase(space),
                                  active: !!data[floorNumber].Men.spaces[space].active,
                                  handicapped: !!data[floorNumber].Men.spaces[space].handicapped,
                                  occupied: !!data[floorNumber].Men.spaces[space].occupied
                              };
                          })
                        : []
                },
                women: {
                    label: 'Women',
                    option: 'gender-women',
                    stalls: data[floorNumber].Women
                        ? Object.keys(data[floorNumber].Women.spaces).map(space => {
                              return {
                                  label: startCase(space),
                                  active: !!data[floorNumber].Women.spaces[space].active,
                                  handicapped: !!data[floorNumber].Women.spaces[space].handicapped,
                                  occupied: !!data[floorNumber].Women.spaces[space].occupied
                              };
                          })
                        : []
                }
            }
        };
    });

    return parsedData;
}

export function extractFloorOptions(data) {
    return Object.keys(data).map(floor => data[floor].option);
}

export function extractGenderOptions() {
    return ['gender-women', 'gender-men'];
}

export function getVisibleStalls(fullData, visibleFloors, visibleGenders, options) {
    if (typeof fullData === 'undefined') {
        return {};
    }

    const onlyHandicapped = !!(options && options.onlyHandicapped);

    return Object.keys(fullData).reduce((visibleData, floor) => {
        if (visibleFloors[`floor-${floor}`]) {
            const potentialGenders = Object.keys(fullData[floor].genders).reduce((genders, gender) => {
                if (visibleGenders[`gender-${gender}`]) {
                    const potentialStalls = onlyHandicapped
                        ? fullData[floor].genders[gender].stalls.filter(stall => stall.handicapped)
                        : fullData[floor].genders[gender].stalls;

                    if (Object.keys(potentialStalls).length) {
                        genders[gender] = {
                            ...fullData[floor].genders[gender],
                            stalls: potentialStalls
                        };
                    }
                }

                return genders;
            }, {});

            if (Object.keys(potentialGenders).length) {
                visibleData[floor] = {
                    ...fullData[floor],
                    genders: potentialGenders
                };
            }
        }

        return visibleData;
    }, {});
}

export function countVisibleStalls(fullData, visibleFloors, visibleGenders, options) {
    const visibleStalls = getVisibleStalls(fullData, visibleFloors, visibleGenders, options);
    if (!visibleStalls || Object.keys(visibleStalls).length === 0) {
        return 0;
    }

    return Object.values(visibleStalls).reduce((totalCount, floor) => {
        return (
            totalCount +
            Object.values(floor.genders).reduce((genderCount, gender) => {
                return (
                    genderCount +
                    Object.values(gender.stalls).reduce(stallCount => {
                        return stallCount + 1;
                    }, 0)
                );
            }, 0)
        );
    }, 0);
}

export function countAvailableStalls(fullData, visibleFloors, visibleGenders, options) {
    const visibleStalls = getVisibleStalls(fullData, visibleFloors, visibleGenders, options);
    if (!visibleStalls || Object.keys(visibleStalls).length === 0) {
        return 0;
    }

    return Object.values(visibleStalls).reduce((totalCount, floor) => {
        return (
            totalCount +
            Object.values(floor.genders).reduce((genderCount, gender) => {
                return (
                    genderCount +
                    Object.values(gender.stalls).reduce((stallCount, stall) => {
                        return stallCount + (stall.active && !stall.occupied ? 1 : 0);
                    }, 0)
                );
            }, 0)
        );
    }, 0);
}

export function countActiveStalls(fullData, visibleFloors, visibleGenders, options) {
    const visibleStalls = getVisibleStalls(fullData, visibleFloors, visibleGenders, options);
    if (!visibleStalls || Object.keys(visibleStalls).length === 0) {
        return 0;
    }

    return Object.values(visibleStalls).reduce((totalCount, floor) => {
        return (
            totalCount +
            Object.values(floor.genders).reduce((genderCount, gender) => {
                return (
                    genderCount +
                    Object.values(gender.stalls).reduce((stallCount, stall) => {
                        return stallCount + (stall.active ? 1 : 0);
                    }, 0)
                );
            }, 0)
        );
    }, 0);
}

export function hasNotification(notifications, floorOption, genderOption) {
    if (!notifications) {
        return false;
    }
    return notifications.findIndex(alert => alert.genderOption === genderOption && alert.floorOption === floorOption) > -1;
}

export function removeNotification(notifications, floorOption, genderOption) {
    if (!notifications) {
        return notifications;
    }

    const alertIndex = notifications.findIndex(alert => alert.genderOption === genderOption && alert.floorOption === floorOption);
    if (alertIndex > -1) {
        notifications.splice(alertIndex, 1);
    }

    return notifications;
}

export function addNotification(notifications, floorOption, genderOption) {
    if (!notifications) {
        return notifications;
    }

    notifications.push({ floorOption, genderOption });
    return notifications;
}

export function updateBadge() {
    window.chrome.storage.local.get(['fullData', 'visibleFloors', 'visibleGenders', 'options'], results => {
        if (results.fullData) {
            const availableStalls = countAvailableStalls(results.fullData, results.visibleFloors, results.visibleGenders, results.options);
            const activeStalls = countActiveStalls(results.fullData, results.visibleFloors, results.visibleGenders, results.options);

            if (activeStalls) {
                if (availableStalls) {
                    window.chrome.browserAction.setBadgeBackgroundColor({ color: [16, 166, 112, 255] });
                } else {
                    window.chrome.browserAction.setBadgeBackgroundColor({ color: [206, 57, 46, 255] });
                }
                window.chrome.browserAction.setBadgeText({ text: availableStalls.toString() });
            } else {
                window.chrome.browserAction.setBadgeText({ text: '' });
            }
        }
    });
}
