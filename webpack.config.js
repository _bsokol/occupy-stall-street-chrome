const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const noop = () => {};

module.exports = env => {
    const isProd = env === 'production';
    const isDev = !isProd;

    return {
        mode: isProd ? 'production' : 'development',

        entry: {
            popup: path.resolve('src/popup/popup.js'),
            options: path.resolve('src/options/options.js'),
            background: path.resolve('src/background/background.js')
        },

        output: {
            path: path.resolve('./dist/occupy-stall-street'),
            publicPath: '/',
            filename: '[name].[chunkhash].js'
        },

        devtool: isDev ? 'inline-source-map' : false,

        module: {
            rules: [
                {
                    enforce: 'pre',
                    exclude: /node_modules/,
                    test: /\.js$/,
                    use: 'eslint-loader'
                },

                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: 'babel-loader'
                },

                {
                    test: /\.(png|jpe?g|gif|woff2?|ttf|eot|ico|svg)$/,
                    use: 'url-loader'
                }
            ]
        },

        optimization: {
            splitChunks: {
                cacheGroups: {
                    commons: {
                        chunks: 'initial',
                        minChunks: 2,
                        name: 'common'
                    }
                }
            }
        },

        plugins: [
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': JSON.stringify(isProd ? 'production' : 'development'),
                __DEV__: JSON.stringify(isDev),
                __PROD__: JSON.stringify(isProd)
            }),

            new CopyWebpackPlugin([
                {
                    from: path.resolve('./src/assets/static')
                }
            ]),

            new webpack.HashedModuleIdsPlugin(),

            new HtmlWebpackPlugin({
                template: path.resolve('./src/popup.html'),
                filename: 'popup.html',
                hash: true,
                inject: 'body',
                minify: {
                    collapseWhitespace: isProd
                },
                chunks: ['common', 'popup']
            }),

            new HtmlWebpackPlugin({
                template: path.resolve('./src/options.html'),
                filename: 'options.html',
                hash: true,
                inject: 'body',
                minify: {
                    collapseWhitespace: isProd
                },
                chunks: ['common', 'options']
            }),

            new HtmlWebpackPlugin({
                template: path.resolve('./src/background.html'),
                filename: 'background.html',
                hash: true,
                inject: 'body',
                minify: {
                    collapseWhitespace: isProd
                },
                chunks: ['common', 'background']
            })
        ],

        stats: {
            children: false,
            chunkModules: false
        },

        resolve: {
            modules: ['./src', 'node_modules'],
            extensions: ['.js', '.jsx']
        },

        target: 'web'
    };

    // if (isProd) {
    //   config.plugins.push(new webpack.optimize.UglifyJsPlugin({
    //     mangle: { screw_ie8: true, keep_fnames: true, mangle_props: false },
    //     compress: { warnings: false, keep_fnames: true, drop_console: true },
    //     output: { comments: false },
    //     sourceMap: true
    //   }));
    // }
};
